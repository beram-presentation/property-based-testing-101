---
title: Property-Based Testing 101
description: Adding a another way of thinking
url: https://beram-presentation.gitlab.io/property-based-testing-101
image: https://beram-presentation.gitlab.io/property-based-testing-101/assets/metadata_image.png
theme: basic
class:
    - lead
paginate: true
footer: |
    @rambaud_b :pig: :heart: :guitar: :musical_note:
    https://gitlab.com/beram-presentation/property-based-testing-101
emoji:
    shortcode: true
style: |
  marp-pre {
    text-align: left;
  }
---

# **Property-Based Testing**
## **101**

🤓 λ :elephant:

---

# Hello! I'm beram :wave:

![bg right auto](assets/avatar.png)

:pig: Benjamin Rambaud
:elephant: PHP Engineer at [@ekino_France](https://twitter.com/ekino_france)

:unicorn:

- Twitter: [@rambaud_b](https://twitter.com/rambaud_b)
- Gitlab: [beram](https://gitlab.com/users/beram/groups)
- Github: [brambaud](https://github.com/brambaud)
- Blog: https://brambaud.github.io/
<!-- - Drupal: [beram](https://drupal.org/u/beram) -->

---

![bg fit](assets/wc_words.png)

---

# 👉 Property-Based Testing 👈

---

🤓 You create a new language 🤓

---

🤓 You create a new language 🤓

# How do you test the addition operation❓
## x + y

---

# ❓

```python
test "add two numbers" {
    assert(1 + 1 === 2);
}
```

---

# ❓

```python 
test "add two numbers" {
    assert(1 + 1 === 2);
    assert(2 + 1 === 3);
    assert(7 + 5 === 12);
    assert(300 + 333 === 666);
    assert(1000 + 1 === 1001);
    assert(25253 + 4782 === 30035);
    assert(1.2 + 2.3 === 3.5)
    // etc...
}
```

---

# Asking a domain expert => a mathematician

- **Commutativity**: `a + b = b + a`

- **Associativity**: `a + b + c = (a + b) + c = a + (b + c)`

- **Identity element is `0`**: `a + 0 = 0 + a = a`

<!--
Associativity: the order of operations does not change the result.

- Special case for integer
  - Successor : for any integer `a`, the integer `(a + 1)` is the least integer greater than `a`
-->

---

```python
test "add two numbers" {
    assert(1 + 1 === 2);
    // etc...
}

@given(a number, b number)
test "addition is commutative" {
    assert(a + b === b + a);
}

@given(a number, b number, c number)
test "addition is associative" {
    assert(a + b + c === (a + b) + c);
    assert((a + b) + c === a + (b + c));
    assert(a + (b + c) === a + b + c);
}

@given(a number)
test "zero is the identity element of the addition" {
    assert(a + 0 === 0 + a);
    assert(a === 0 + a);
    assert(a + 0 === a);
}
```

---

![bg auto right](assets/tennant_olympics.jpeg)

# That's it 🤓

Asserting that invariants/properties are always true given specific conditions 

---

# Common properties 🔍

- Commutativity
- Associativity
- Identity element
- Successor
- Reversibility
- Some things never change
- Idempotence
- Structural induction: Solve a smaller problem first
- Test Oracle: Use an alternate version to check the result

---

# Common properties 🔍
## Reversibility

`data === decode(encode(data))`

---

# Common properties 🔍
## Some things never change

an invariant that is preserved after some transformation

`length(collection) === length(sort(collection))`

---

# Common properties 🔍
## Idempotence

given the same input, doing an operation twice is the same as doing it once

---

# Common properties 🔍

Speak to a domain expert to find them 🤓

---

# Is that really it❓
## No I lied 😁

- generate lots of random cases 🥸
- describe the cases ✍️
- shrink to find the minimal case it fails 👶

<!-- 
shrink: tries to reduce it to a minimal failing subset by removing or simplifying input data that are unneeded to make the test fail.
-->

--- 

# Tooling 🛠

<div class="tooling-column">
<div class="tooling-column-1">
<h2>Others</h2>

- [QuickCheck](https://github.com/nick8325/quickcheck) (Haskell)
- [testing/quick](https://pkg.go.dev/testing/quick) (Go native)
- [StreamData](https://hexdocs.pm/stream_data/StreamData.html) (Elixir )
- [Hypothesis](https://hypothesis.readthedocs.io/) (Python)
- [QuickCheck](https://crates.io/crates/quickcheck) (Rust)
- [...]
</div>

<div class="tooling-column-2">
<h2>PHP 🐘</h2>

- [BlackBox](https://github.com/Innmind/BlackBox)
- [Eris](https://github.com/giorgiosironi/eris)
- [PHPQuickCheck](https://github.com/steos/php-quickcheck)
- Faker and others factories or builder on your project
</div>
</div>

<style>
.tooling-column {
  display: inline-flex;
} 
.tooling-column-1 {
    width: 50%;
}
.tooling-column-2 {
    width: 50%;
} 
</style>


---

# Conclusion 🏁

- Complement all others technics, patterns, etc..
- Help to prove "it works"
- Help to find when "it doesn't work"

---

# More readings 🤓

- https://www.infoq.com/presentations/testing-techniques-case-study/
- http://web.archive.org/web/20140928140255/http://research.microsoft.com/en-us/projects/pex/patterns.pdf
- https://fsharpforfunandprofit.com/posts/property-based-testing-2/
- https://dev.to/youroff/a-case-study-for-property-based-testing-in-elixir-44c6
- https://www.youtube.com/watch?v=shngiiBfD80
- https://hypothesis.readthedocs.io/en/latest/examples.html
- https://hypothesis.readthedocs.io/en/latest/endorsements.html

---

# Thanks! Any Questions?

![auto](assets/banana-beram.gif)
