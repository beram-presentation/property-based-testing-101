# Property-Based Testing 101

Let's discover what is property-based testing!

## Installation

Those slides are written in Markdown thanks to [Marp](https://marp.app/)
(a Markdown presentation ecosystem).

I, personally, choose to use the
[official marp-cli Docker image](https://hub.docker.com/r/marpteam/marp-cli).
So the installation process only describe this way.
Nevertheless, feel free to use another way :)

Clone this repository and go inside:

```bash
git clone [REPOSITORY_URL] php-property-based-testing-101 && cd $_
```

Check the `Makefile` to see the available options.
Default is to run `marp-cli` in server mode:

```bash
make
```
