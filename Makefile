.DEFAULT_GOAL := serve

SLIDES_DIR = slides
BUILD_DIR = public

MARP_CLI_VERSION = v4.1.2
MARP_SERVE_PORT ?= 8080
MARP_WATCH_PORT ?= 37717
MARP_CLI_FLAG_INPUT_DIR = --input-dir $(SLIDES_DIR)
MARP_CLI_FLAG_OUTPUT_DIR = --output $(BUILD_DIR)
MARP_CLI_CONVERT_FLAG = $(MARP_CLI_FLAG_INPUT_DIR) $(MARP_CLI_FLAG_OUTPUT_DIR) --allow-local-files
MARP_CLI_COMMON_FLAG = --html
MARP_CLI ?= docker run --rm --init \
	-v $(PWD):/home/marp/app \
	-e LANG=en_US.UTF-8 \
	-p $(MARP_SERVE_PORT):8080 \
	-p $(MARP_WATCH_PORT):37717 \
	marpteam/marp-cli:$(MARP_CLI_VERSION)

.PHONY: serve
serve:
	$(MARP_CLI) $(MARP_CLI_FLAG_INPUT_DIR) $(MARP_CLI_COMMON_FLAG) --bespoke.progress  -s

.PHONY: convert-pdf
convert-pdf:
	$(MARP_CLI) $(MARP_CLI_CONVERT_FLAG) $(MARP_CLI_COMMON_FLAG) --pdf

.PHONY: convert-pptx
convert-pptx:
	$(MARP_CLI) $(MARP_CLI_CONVERT_FLAG) $(MARP_CLI_COMMON_FLAG) --pptx

.PHONY: convert-html
convert-html:
	$(MARP_CLI) $(MARP_CLI_CONVERT_FLAG) $(MARP_CLI_COMMON_FLAG)
	cp -R $(SLIDES_DIR)/assets $(BUILD_DIR)/.
